﻿using System;
using System.Collections.Generic;

namespace projectAdminPanel.Models
{
    public partial class TaskAssignDetail
    {
        public int TaskAssignDetailId { get; set; }
        public DateTime Starting { get; set; }
        public DateTime Ending { get; set; }
        public string TaskDescription { get; set; }
        public int CompletionPercent { get; set; }
        public int TaskAssignId { get; set; }

        public virtual TaskAssign TaskAssign { get; set; }
    }
}
