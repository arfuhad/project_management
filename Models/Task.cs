﻿using System;
using System.Collections.Generic;

namespace projectAdminPanel.Models
{
    public partial class Task
    {
        public Task()
        {
            TaskAssign = new HashSet<TaskAssign>();
        }

        public int TaskId { get; set; }
        public string TaskDescirption { get; set; }
        public DateTime TaskDate { get; set; }
        public int WorkId { get; set; }
        public int TaskTypeId { get; set; }

        public virtual TaskType TaskType { get; set; }
        public virtual Work Work { get; set; }
        public virtual ICollection<TaskAssign> TaskAssign { get; set; }
    }
}
