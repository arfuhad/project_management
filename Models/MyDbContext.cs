﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace projectAdminPanel.Models
{
    public partial class MyDbContext : DbContext
    {
        public MyDbContext()
        {
        }

        public MyDbContext(DbContextOptions<MyDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Organization> Organization { get; set; }
        public virtual DbSet<OrganizationType> OrganizationType { get; set; }
        public virtual DbSet<Project> Project { get; set; }
        public virtual DbSet<ProjectType> ProjectType { get; set; }
        public virtual DbSet<Resource> Resource { get; set; }
        public virtual DbSet<ResourceHierarchy> ResourceHierarchy { get; set; }
        public virtual DbSet<ResourceType> ResourceType { get; set; }
        public virtual DbSet<Task> Task { get; set; }
        public virtual DbSet<TaskAssign> TaskAssign { get; set; }
        public virtual DbSet<TaskAssignDetail> TaskAssignDetail { get; set; }
        public virtual DbSet<TaskType> TaskType { get; set; }
        public virtual DbSet<Work> Work { get; set; }
        public virtual DbSet<WorkAssign> WorkAssign { get; set; }
        public virtual DbSet<WorkType> WorkType { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Organization>(entity =>
            {
                entity.HasKey(e => e.OrgId)
                    .HasName("PRIMARY");

                entity.ToTable("organization");

                entity.HasIndex(e => e.OrgTypeId)
                    .HasName("org_table_org_type_table");

                entity.Property(e => e.OrgId)
                    .HasColumnName("org_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.OrgName)
                    .IsRequired()
                    .HasColumnName("org_name")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.OrgTypeId)
                    .HasColumnName("org_type_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.OrgType)
                    .WithMany(p => p.Organization)
                    .HasForeignKey(d => d.OrgTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("org_table_org_type_table");
            });

            modelBuilder.Entity<OrganizationType>(entity =>
            {
                entity.HasKey(e => e.OrgTypeId)
                    .HasName("PRIMARY");

                entity.ToTable("organization_type");

                entity.Property(e => e.OrgTypeId)
                    .HasColumnName("org_type_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.OrgTypeName)
                    .IsRequired()
                    .HasColumnName("org_type_name")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<Project>(entity =>
            {
                entity.HasKey(e => e.ProjId)
                    .HasName("PRIMARY");

                entity.ToTable("project");

                entity.HasIndex(e => e.OrgId)
                    .HasName("project_table_org_table");

                entity.HasIndex(e => e.ProjTypeId)
                    .HasName("project_table_proj_type_table");

                entity.Property(e => e.ProjId)
                    .HasColumnName("proj_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.OrgId)
                    .HasColumnName("org_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProjDetails)
                    .IsRequired()
                    .HasColumnName("proj_details")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ProjName)
                    .IsRequired()
                    .HasColumnName("proj_name")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ProjTypeId)
                    .HasColumnName("proj_type_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Org)
                    .WithMany(p => p.Project)
                    .HasForeignKey(d => d.OrgId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("project_table_org_table");

                entity.HasOne(d => d.ProjType)
                    .WithMany(p => p.Project)
                    .HasForeignKey(d => d.ProjTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("project_table_proj_type_table");
            });

            modelBuilder.Entity<ProjectType>(entity =>
            {
                entity.HasKey(e => e.ProjTypeId)
                    .HasName("PRIMARY");

                entity.ToTable("project_type");

                entity.Property(e => e.ProjTypeId)
                    .HasColumnName("proj_type_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProjTypeName)
                    .IsRequired()
                    .HasColumnName("proj_type_name")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<Resource>(entity =>
            {
                entity.HasKey(e => e.ResId)
                    .HasName("PRIMARY");

                entity.ToTable("resource");

                entity.HasIndex(e => e.OrgId)
                    .HasName("resource_table_org_table");

                entity.HasIndex(e => e.ResHierId)
                    .HasName("resource_resource_hierarchy");

                entity.HasIndex(e => e.ResTypeId)
                    .HasName("resource_table_res_type_table");

                entity.Property(e => e.ResId)
                    .HasColumnName("res_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.OrgId)
                    .HasColumnName("org_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ResEmail)
                    .IsRequired()
                    .HasColumnName("res_email")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ResHierId)
                    .HasColumnName("res_hier_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ResMobile)
                    .IsRequired()
                    .HasColumnName("res_mobile")
                    .HasColumnType("varchar(11)");

                entity.Property(e => e.ResName)
                    .IsRequired()
                    .HasColumnName("res_name")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ResPassword)
                    .IsRequired()
                    .HasColumnName("res_password")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ResTypeId)
                    .HasColumnName("res_type_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Org)
                    .WithMany(p => p.Resource)
                    .HasForeignKey(d => d.OrgId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("resource_table_org_table");

                entity.HasOne(d => d.ResHier)
                    .WithMany(p => p.Resource)
                    .HasForeignKey(d => d.ResHierId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("resource_resource_hierarchy");

                entity.HasOne(d => d.ResType)
                    .WithMany(p => p.Resource)
                    .HasForeignKey(d => d.ResTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("resource_table_res_type_table");
            });

            modelBuilder.Entity<ResourceHierarchy>(entity =>
            {
                entity.HasKey(e => e.ResHierId)
                    .HasName("PRIMARY");

                entity.ToTable("resource_hierarchy");

                entity.Property(e => e.ResHierId)
                    .HasColumnName("res_hier_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ResHierName)
                    .IsRequired()
                    .HasColumnName("res_hier_name")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<ResourceType>(entity =>
            {
                entity.HasKey(e => e.ResTypeId)
                    .HasName("PRIMARY");

                entity.ToTable("resource_type");

                entity.Property(e => e.ResTypeId)
                    .HasColumnName("res_type_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ResTypeName)
                    .IsRequired()
                    .HasColumnName("res_type_name")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<Task>(entity =>
            {
                entity.ToTable("task");

                entity.HasIndex(e => e.TaskTypeId)
                    .HasName("task_2_task_type_table");

                entity.HasIndex(e => e.WorkId)
                    .HasName("task_2_work");

                entity.Property(e => e.TaskId)
                    .HasColumnName("task_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TaskDate)
                    .HasColumnName("task_date")
                    .HasColumnType("date");

                entity.Property(e => e.TaskDescirption)
                    .IsRequired()
                    .HasColumnName("task_descirption")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.TaskTypeId)
                    .HasColumnName("task_type_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.WorkId)
                    .HasColumnName("work_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.TaskType)
                    .WithMany(p => p.Task)
                    .HasForeignKey(d => d.TaskTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("task_2_task_type_table");

                entity.HasOne(d => d.Work)
                    .WithMany(p => p.Task)
                    .HasForeignKey(d => d.WorkId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("task_2_work");
            });

            modelBuilder.Entity<TaskAssign>(entity =>
            {
                entity.ToTable("task_assign");

                entity.HasIndex(e => e.ResId)
                    .HasName("task_assign_resource");

                entity.HasIndex(e => e.TaskId)
                    .HasName("task_assign_task_2");

                entity.Property(e => e.TaskAssignId)
                    .HasColumnName("task_assign_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ResId)
                    .HasColumnName("res_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TaskAssignDate)
                    .HasColumnName("task_assign_date")
                    .HasColumnType("date");

                entity.Property(e => e.TaskId)
                    .HasColumnName("task_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Res)
                    .WithMany(p => p.TaskAssign)
                    .HasForeignKey(d => d.ResId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("task_assign_resource");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.TaskAssign)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("task_assign_task_2");
            });

            modelBuilder.Entity<TaskAssignDetail>(entity =>
            {
                entity.ToTable("task_assign_detail");

                entity.HasIndex(e => e.TaskAssignId)
                    .HasName("task_assign_detail_task_assign");

                entity.Property(e => e.TaskAssignDetailId)
                    .HasColumnName("task_assign_detail_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CompletionPercent)
                    .HasColumnName("completion_percent")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Ending)
                    .HasColumnName("ending")
                    .HasColumnType("datetime");

                entity.Property(e => e.Starting)
                    .HasColumnName("starting")
                    .HasColumnType("datetime");

                entity.Property(e => e.TaskAssignId)
                    .HasColumnName("task_assign_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TaskDescription)
                    .IsRequired()
                    .HasColumnName("task_description")
                    .HasColumnType("varchar(255)");

                entity.HasOne(d => d.TaskAssign)
                    .WithMany(p => p.TaskAssignDetail)
                    .HasForeignKey(d => d.TaskAssignId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("task_assign_detail_task_assign");
            });

            modelBuilder.Entity<TaskType>(entity =>
            {
                entity.ToTable("task_type");

                entity.Property(e => e.TaskTypeId)
                    .HasColumnName("task_type_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TaskTypeName)
                    .IsRequired()
                    .HasColumnName("task_type_name")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<Work>(entity =>
            {
                entity.ToTable("work");

                entity.HasIndex(e => e.ProjId)
                    .HasName("work_project_table");

                entity.HasIndex(e => e.WorkTypeId)
                    .HasName("work_work_type_table");

                entity.Property(e => e.WorkId)
                    .HasColumnName("work_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProjId)
                    .HasColumnName("proj_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.WorkDate)
                    .HasColumnName("work_date")
                    .HasColumnType("date");

                entity.Property(e => e.WorkDescirption)
                    .IsRequired()
                    .HasColumnName("work_descirption")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.WorkTypeId)
                    .HasColumnName("work_type_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Proj)
                    .WithMany(p => p.Work)
                    .HasForeignKey(d => d.ProjId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("work_project_table");

                entity.HasOne(d => d.WorkType)
                    .WithMany(p => p.Work)
                    .HasForeignKey(d => d.WorkTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("work_work_type_table");
            });

            modelBuilder.Entity<WorkAssign>(entity =>
            {
                entity.ToTable("work_assign");

                entity.HasIndex(e => e.ResId)
                    .HasName("work_assign_resource_table");

                entity.HasIndex(e => e.WorkId)
                    .HasName("work_assign_work");

                entity.Property(e => e.WorkAssignId)
                    .HasColumnName("work_assign_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ResId)
                    .HasColumnName("res_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.WorkAssignDate)
                    .HasColumnName("work_assign_date")
                    .HasColumnType("date");

                entity.Property(e => e.WorkId)
                    .HasColumnName("work_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Res)
                    .WithMany(p => p.WorkAssign)
                    .HasForeignKey(d => d.ResId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("work_assign_resource_table");

                entity.HasOne(d => d.Work)
                    .WithMany(p => p.WorkAssign)
                    .HasForeignKey(d => d.WorkId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("work_assign_work");
            });

            modelBuilder.Entity<WorkType>(entity =>
            {
                entity.ToTable("work_type");

                entity.Property(e => e.WorkTypeId)
                    .HasColumnName("work_type_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.WorkTypeName)
                    .IsRequired()
                    .HasColumnName("work_type_name")
                    .HasColumnType("varchar(50)");
            });
        }
    }
}
