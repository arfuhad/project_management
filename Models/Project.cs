﻿using System;
using System.Collections.Generic;

namespace projectAdminPanel.Models
{
    public partial class Project
    {
        public Project()
        {
            Work = new HashSet<Work>();
        }

        public int ProjId { get; set; }
        public string ProjName { get; set; }
        public string ProjDetails { get; set; }
        public int ProjTypeId { get; set; }
        public int OrgId { get; set; }

        public virtual Organization Org { get; set; }
        public virtual ProjectType ProjType { get; set; }
        public virtual ICollection<Work> Work { get; set; }
    }
}
