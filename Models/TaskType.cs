﻿using System;
using System.Collections.Generic;

namespace projectAdminPanel.Models
{
    public partial class TaskType
    {
        public TaskType()
        {
            Task = new HashSet<Task>();
        }

        public int TaskTypeId { get; set; }
        public string TaskTypeName { get; set; }

        public virtual ICollection<Task> Task { get; set; }
    }
}
