﻿using System;
using System.Collections.Generic;

namespace projectAdminPanel.Models
{
    public partial class WorkAssign
    {
        public int WorkAssignId { get; set; }
        public DateTime WorkAssignDate { get; set; }
        public int WorkId { get; set; }
        public int? ResId { get; set; }

        public virtual Resource Res { get; set; }
        public virtual Work Work { get; set; }
    }
}
