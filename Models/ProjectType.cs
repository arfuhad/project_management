﻿using System;
using System.Collections.Generic;

namespace projectAdminPanel.Models
{
    public partial class ProjectType
    {
        public ProjectType()
        {
            Project = new HashSet<Project>();
        }

        public int ProjTypeId { get; set; }
        public string ProjTypeName { get; set; }

        public virtual ICollection<Project> Project { get; set; }
    }
}
