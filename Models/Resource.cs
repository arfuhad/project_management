﻿using System;
using System.Collections.Generic;

namespace projectAdminPanel.Models
{
    public partial class Resource
    {
        public Resource()
        {
            TaskAssign = new HashSet<TaskAssign>();
            WorkAssign = new HashSet<WorkAssign>();
        }

        public int ResId { get; set; }
        public string ResName { get; set; }
        public string ResEmail { get; set; }
        public string ResPassword { get; set; }
        public string ResMobile { get; set; }
        public int ResTypeId { get; set; }
        public int ResHierId { get; set; }
        public int OrgId { get; set; }

        public virtual Organization Org { get; set; }
        public virtual ResourceHierarchy ResHier { get; set; }
        public virtual ResourceType ResType { get; set; }
        public virtual ICollection<TaskAssign> TaskAssign { get; set; }
        public virtual ICollection<WorkAssign> WorkAssign { get; set; }
    }
}
