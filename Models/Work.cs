﻿using System;
using System.Collections.Generic;

namespace projectAdminPanel.Models
{
    public partial class Work
    {
        public Work()
        {
            Task = new HashSet<Task>();
            WorkAssign = new HashSet<WorkAssign>();
        }

        public int WorkId { get; set; }
        public string WorkDescirption { get; set; }
        public DateTime WorkDate { get; set; }
        public int ProjId { get; set; }
        public int WorkTypeId { get; set; }

        public virtual Project Proj { get; set; }
        public virtual WorkType WorkType { get; set; }
        public virtual ICollection<Task> Task { get; set; }
        public virtual ICollection<WorkAssign> WorkAssign { get; set; }
    }
}
