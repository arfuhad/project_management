﻿using System;
using System.Collections.Generic;

namespace projectAdminPanel.Models
{
    public partial class OrganizationType
    {
        public OrganizationType()
        {
            Organization = new HashSet<Organization>();
        }

        public int OrgTypeId { get; set; }
        public string OrgTypeName { get; set; }

        public virtual ICollection<Organization> Organization { get; set; }
    }
}
