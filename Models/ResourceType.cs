﻿using System;
using System.Collections.Generic;

namespace projectAdminPanel.Models
{
    public partial class ResourceType
    {
        public ResourceType()
        {
            Resource = new HashSet<Resource>();
        }

        public int ResTypeId { get; set; }
        public string ResTypeName { get; set; }

        public virtual ICollection<Resource> Resource { get; set; }
    }
}
