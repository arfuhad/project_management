﻿using System;
using System.Collections.Generic;

namespace projectAdminPanel.Models
{
    public partial class ResourceHierarchy
    {
        public ResourceHierarchy()
        {
            Resource = new HashSet<Resource>();
        }

        public int ResHierId { get; set; }
        public string ResHierName { get; set; }

        public virtual ICollection<Resource> Resource { get; set; }
    }
}
