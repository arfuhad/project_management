﻿using System;
using System.Collections.Generic;

namespace projectAdminPanel.Models
{
    public partial class Organization
    {
        public Organization()
        {
            Project = new HashSet<Project>();
            Resource = new HashSet<Resource>();
        }

        public int OrgId { get; set; }
        public string OrgName { get; set; }
        public int OrgTypeId { get; set; }

        public virtual OrganizationType OrgType { get; set; }
        public virtual ICollection<Project> Project { get; set; }
        public virtual ICollection<Resource> Resource { get; set; }
    }
}
