﻿using System;
using System.Collections.Generic;

namespace projectAdminPanel.Models
{
    public partial class WorkType
    {
        public WorkType()
        {
            Work = new HashSet<Work>();
        }

        public int WorkTypeId { get; set; }
        public string WorkTypeName { get; set; }

        public virtual ICollection<Work> Work { get; set; }
    }
}
