﻿using System;
using System.Collections.Generic;

namespace projectAdminPanel.Models
{
    public partial class TaskAssign
    {
        public TaskAssign()
        {
            TaskAssignDetail = new HashSet<TaskAssignDetail>();
        }

        public int TaskAssignId { get; set; }
        public DateTime TaskAssignDate { get; set; }
        public int TaskId { get; set; }
        public int ResId { get; set; }

        public virtual Resource Res { get; set; }
        public virtual Task Task { get; set; }
        public virtual ICollection<TaskAssignDetail> TaskAssignDetail { get; set; }
    }
}
