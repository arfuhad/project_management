-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 12, 2019 at 02:17 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE `organization` (
  `org_id` int(11) NOT NULL,
  `org_name` varchar(50) NOT NULL,
  `org_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`org_id`, `org_name`, `org_type_id`) VALUES
(1, 'Admin', 1),
(2, 'Edusoft Consultants Ltd', 3),
(3, 'United International University', 4);

-- --------------------------------------------------------

--
-- Table structure for table `organization_type`
--

CREATE TABLE `organization_type` (
  `org_type_id` int(11) NOT NULL,
  `org_type_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `organization_type`
--

INSERT INTO `organization_type` (`org_type_id`, `org_type_name`) VALUES
(1, 'Admin'),
(2, 'Institutional'),
(3, 'IT'),
(4, 'Educational'),
(5, 'Organizational'),
(6, 'Corporate');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `proj_id` int(11) NOT NULL,
  `proj_name` varchar(50) NOT NULL,
  `proj_details` varchar(50) NOT NULL,
  `proj_type_id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`proj_id`, `proj_name`, `proj_details`, `proj_type_id`, `org_id`) VALUES
(1, 'Mangement System', 'Create a management system for enterprises', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `project_type`
--

CREATE TABLE `project_type` (
  `proj_type_id` int(11) NOT NULL,
  `proj_type_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `project_type`
--

INSERT INTO `project_type` (`proj_type_id`, `proj_type_name`) VALUES
(1, 'Major'),
(2, 'Intermediate'),
(4, 'Essential');

-- --------------------------------------------------------

--
-- Table structure for table `resource`
--

CREATE TABLE `resource` (
  `res_id` int(11) NOT NULL,
  `res_name` varchar(50) NOT NULL,
  `res_email` varchar(50) NOT NULL,
  `res_password` varchar(50) NOT NULL,
  `res_mobile` varchar(11) NOT NULL,
  `res_type_id` int(11) NOT NULL,
  `res_hier_id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `resource`
--

INSERT INTO `resource` (`res_id`, `res_name`, `res_email`, `res_password`, `res_mobile`, `res_type_id`, `res_hier_id`, `org_id`) VALUES
(1, 'Admin', 'admin@edusoft.com', 'adminedu', '01687671007', 1, 1, 1),
(2, 'Nasirul Amin Pratik', 'pratik@gmail.com', 'pratik', '01644506078', 3, 2, 2),
(3, 'fuhad', 'fuhad@gmail.com', '123456', '01687671007', 1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `resource_hierarchy`
--

CREATE TABLE `resource_hierarchy` (
  `res_hier_id` int(11) NOT NULL,
  `res_hier_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `resource_hierarchy`
--

INSERT INTO `resource_hierarchy` (`res_hier_id`, `res_hier_name`) VALUES
(1, 'Very High'),
(2, 'High'),
(3, 'Avarage'),
(4, 'Low'),
(5, 'Very Low');

-- --------------------------------------------------------

--
-- Table structure for table `resource_type`
--

CREATE TABLE `resource_type` (
  `res_type_id` int(11) NOT NULL,
  `res_type_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `resource_type`
--

INSERT INTO `resource_type` (`res_type_id`, `res_type_name`) VALUES
(1, 'Admin'),
(2, 'Manager'),
(3, 'Senior Resource'),
(4, 'Junior Resource'),
(5, 'internship');

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `task_id` int(11) NOT NULL,
  `task_descirption` varchar(50) NOT NULL,
  `task_date` date NOT NULL,
  `work_id` int(11) NOT NULL,
  `task_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`task_id`, `task_descirption`, `task_date`, `work_id`, `task_type_id`) VALUES
(1, 'UI Planning', '2019-11-04', 2, 1),
(2, 'Database Planning', '2019-11-11', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `task_assign`
--

CREATE TABLE `task_assign` (
  `task_assign_id` int(11) NOT NULL,
  `task_assign_date` date NOT NULL,
  `task_id` int(11) NOT NULL,
  `res_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `task_assign`
--

INSERT INTO `task_assign` (`task_assign_id`, `task_assign_date`, `task_id`, `res_id`) VALUES
(1, '2019-11-05', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `task_assign_detail`
--

CREATE TABLE `task_assign_detail` (
  `task_assign_detail_id` int(11) NOT NULL,
  `starting` datetime NOT NULL,
  `ending` datetime NOT NULL,
  `task_description` varchar(255) NOT NULL,
  `completion_percent` int(11) NOT NULL,
  `task_assign_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `task_assign_detail`
--

INSERT INTO `task_assign_detail` (`task_assign_detail_id`, `starting`, `ending`, `task_description`, `completion_percent`, `task_assign_id`) VALUES
(8, '2019-11-03 01:00:00', '2019-11-03 04:00:00', 'sketch ready', 44, 1);

-- --------------------------------------------------------

--
-- Table structure for table `task_type`
--

CREATE TABLE `task_type` (
  `task_type_id` int(11) NOT NULL,
  `task_type_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `task_type`
--

INSERT INTO `task_type` (`task_type_id`, `task_type_name`) VALUES
(1, 'Argent'),
(2, 'Modarates'),
(3, 'Flexible');

-- --------------------------------------------------------

--
-- Table structure for table `work`
--

CREATE TABLE `work` (
  `work_id` int(11) NOT NULL,
  `work_descirption` varchar(50) NOT NULL,
  `work_date` date NOT NULL,
  `proj_id` int(11) NOT NULL,
  `work_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `work`
--

INSERT INTO `work` (`work_id`, `work_descirption`, `work_date`, `proj_id`, `work_type_id`) VALUES
(2, 'Planning for features', '2019-11-02', 1, 1),
(3, 'ready for features', '2019-11-18', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `work_assign`
--

CREATE TABLE `work_assign` (
  `work_assign_id` int(11) NOT NULL,
  `work_assign_date` date NOT NULL,
  `work_id` int(11) NOT NULL,
  `res_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `work_assign`
--

INSERT INTO `work_assign` (`work_assign_id`, `work_assign_date`, `work_id`, `res_id`) VALUES
(1, '2019-11-18', 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `work_type`
--

CREATE TABLE `work_type` (
  `work_type_id` int(11) NOT NULL,
  `work_type_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `work_type`
--

INSERT INTO `work_type` (`work_type_id`, `work_type_name`) VALUES
(1, 'Strategic'),
(2, 'Preparation'),
(3, 'Concept Design'),
(4, 'Developed Design'),
(5, 'Technical Design'),
(7, 'Construction'),
(8, 'Handover');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `organization`
--
ALTER TABLE `organization`
  ADD PRIMARY KEY (`org_id`),
  ADD KEY `org_table_org_type_table` (`org_type_id`);

--
-- Indexes for table `organization_type`
--
ALTER TABLE `organization_type`
  ADD PRIMARY KEY (`org_type_id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`proj_id`),
  ADD KEY `project_table_org_table` (`org_id`),
  ADD KEY `project_table_proj_type_table` (`proj_type_id`);

--
-- Indexes for table `project_type`
--
ALTER TABLE `project_type`
  ADD PRIMARY KEY (`proj_type_id`);

--
-- Indexes for table `resource`
--
ALTER TABLE `resource`
  ADD PRIMARY KEY (`res_id`),
  ADD KEY `resource_resource_hierarchy` (`res_hier_id`),
  ADD KEY `resource_table_org_table` (`org_id`),
  ADD KEY `resource_table_res_type_table` (`res_type_id`);

--
-- Indexes for table `resource_hierarchy`
--
ALTER TABLE `resource_hierarchy`
  ADD PRIMARY KEY (`res_hier_id`);

--
-- Indexes for table `resource_type`
--
ALTER TABLE `resource_type`
  ADD PRIMARY KEY (`res_type_id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`task_id`),
  ADD KEY `task_2_task_type_table` (`task_type_id`),
  ADD KEY `task_2_work` (`work_id`);

--
-- Indexes for table `task_assign`
--
ALTER TABLE `task_assign`
  ADD PRIMARY KEY (`task_assign_id`),
  ADD KEY `task_assign_resource` (`res_id`),
  ADD KEY `task_assign_task_2` (`task_id`);

--
-- Indexes for table `task_assign_detail`
--
ALTER TABLE `task_assign_detail`
  ADD PRIMARY KEY (`task_assign_detail_id`),
  ADD KEY `task_assign_detail_task_assign` (`task_assign_id`);

--
-- Indexes for table `task_type`
--
ALTER TABLE `task_type`
  ADD PRIMARY KEY (`task_type_id`);

--
-- Indexes for table `work`
--
ALTER TABLE `work`
  ADD PRIMARY KEY (`work_id`),
  ADD KEY `work_project_table` (`proj_id`),
  ADD KEY `work_work_type_table` (`work_type_id`);

--
-- Indexes for table `work_assign`
--
ALTER TABLE `work_assign`
  ADD PRIMARY KEY (`work_assign_id`),
  ADD KEY `work_assign_resource_table` (`res_id`),
  ADD KEY `work_assign_work` (`work_id`);

--
-- Indexes for table `work_type`
--
ALTER TABLE `work_type`
  ADD PRIMARY KEY (`work_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `organization`
--
ALTER TABLE `organization`
  MODIFY `org_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `organization_type`
--
ALTER TABLE `organization_type`
  MODIFY `org_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `proj_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `project_type`
--
ALTER TABLE `project_type`
  MODIFY `proj_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `resource`
--
ALTER TABLE `resource`
  MODIFY `res_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `resource_hierarchy`
--
ALTER TABLE `resource_hierarchy`
  MODIFY `res_hier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `resource_type`
--
ALTER TABLE `resource_type`
  MODIFY `res_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `task_assign`
--
ALTER TABLE `task_assign`
  MODIFY `task_assign_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `task_assign_detail`
--
ALTER TABLE `task_assign_detail`
  MODIFY `task_assign_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `task_type`
--
ALTER TABLE `task_type`
  MODIFY `task_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `work`
--
ALTER TABLE `work`
  MODIFY `work_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `work_assign`
--
ALTER TABLE `work_assign`
  MODIFY `work_assign_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `work_type`
--
ALTER TABLE `work_type`
  MODIFY `work_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `organization`
--
ALTER TABLE `organization`
  ADD CONSTRAINT `org_table_org_type_table` FOREIGN KEY (`org_type_id`) REFERENCES `organization_type` (`org_type_id`);

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `project_table_org_table` FOREIGN KEY (`org_id`) REFERENCES `organization` (`org_id`),
  ADD CONSTRAINT `project_table_proj_type_table` FOREIGN KEY (`proj_type_id`) REFERENCES `project_type` (`proj_type_id`);

--
-- Constraints for table `resource`
--
ALTER TABLE `resource`
  ADD CONSTRAINT `resource_resource_hierarchy` FOREIGN KEY (`res_hier_Id`) REFERENCES `resource_hierarchy` (`res_hier_Id`),
  ADD CONSTRAINT `resource_table_org_table` FOREIGN KEY (`org_id`) REFERENCES `organization` (`org_id`),
  ADD CONSTRAINT `resource_table_res_type_table` FOREIGN KEY (`res_type_id`) REFERENCES `resource_type` (`res_type_id`);

--
-- Constraints for table `task`
--
ALTER TABLE `task`
  ADD CONSTRAINT `task_2_task_type_table` FOREIGN KEY (`task_type_id`) REFERENCES `task_type` (`task_type_id`),
  ADD CONSTRAINT `task_2_work` FOREIGN KEY (`work_id`) REFERENCES `work` (`work_id`);

--
-- Constraints for table `task_assign`
--
ALTER TABLE `task_assign`
  ADD CONSTRAINT `task_assign_resource` FOREIGN KEY (`res_id`) REFERENCES `resource` (`res_id`),
  ADD CONSTRAINT `task_assign_task_2` FOREIGN KEY (`task_id`) REFERENCES `task` (`task_id`);

--
-- Constraints for table `task_assign_detail`
--
ALTER TABLE `task_assign_detail`
  ADD CONSTRAINT `task_assign_detail_task_assign` FOREIGN KEY (`task_assign_id`) REFERENCES `task_assign` (`task_assign_id`);

--
-- Constraints for table `work`
--
ALTER TABLE `work`
  ADD CONSTRAINT `work_project_table` FOREIGN KEY (`proj_id`) REFERENCES `project` (`proj_id`),
  ADD CONSTRAINT `work_work_type_table` FOREIGN KEY (`work_type_id`) REFERENCES `work_type` (`work_type_id`);

--
-- Constraints for table `work_assign`
--
ALTER TABLE `work_assign`
  ADD CONSTRAINT `work_assign_resource_table` FOREIGN KEY (`res_id`) REFERENCES `resource` (`res_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `work_assign_work` FOREIGN KEY (`work_id`) REFERENCES `work` (`work_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
