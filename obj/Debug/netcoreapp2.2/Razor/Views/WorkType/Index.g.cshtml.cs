#pragma checksum "/Users/fuhad/Desktop/projectAdminPanel/Views/WorkType/Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6943f3f660ef052d4db2093f9f4c41b3ece68427"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_WorkType_Index), @"mvc.1.0.view", @"/Views/WorkType/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/WorkType/Index.cshtml", typeof(AspNetCore.Views_WorkType_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6943f3f660ef052d4db2093f9f4c41b3ece68427", @"/Views/WorkType/Index.cshtml")]
    public class Views_WorkType_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<projectAdminPanel.Models.WorkType>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/bower_components/datatables.net/js/jquery.dataTables.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(54, 740, true);
            WriteLiteral(@"
<section class=""content-header"">
    <h1>
    Work Type
    </h1>
    <ol class=""breadcrumb"">
    <li><a href=""#""><i class=""fa fa-dashboard""></i> Home</a></li>
    <li class=""active"">Work Type</li>
    </ol>
</section>
<!-- Main content -->
<section class=""content"">
    <div class=""row"">
    <div class=""col-xs-12"">
        <div class=""box"">
        <div class=""box-header"">
            <h3 class=""box-title"">Manage Work Type</h3>
        </div>
        <!-- /.box-header -->
        <div class=""box-body"">
            <table id=""example1"" class=""table table-bordered table-striped"">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
            </tr>
            </thead>
            <tbody>
");
            EndContext();
#line 30 "/Users/fuhad/Desktop/projectAdminPanel/Views/WorkType/Index.cshtml"
                 foreach (var item in Model)
                {

#line default
#line hidden
            BeginContext(857, 49, true);
            WriteLiteral("                    <tr>\n                    <td>");
            EndContext();
            BeginContext(907, 45, false);
#line 33 "/Users/fuhad/Desktop/projectAdminPanel/Views/WorkType/Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.WorkTypeId));

#line default
#line hidden
            EndContext();
            BeginContext(952, 30, true);
            WriteLiteral("</td>\n                    <td>");
            EndContext();
            BeginContext(983, 47, false);
#line 34 "/Users/fuhad/Desktop/projectAdminPanel/Views/WorkType/Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.WorkTypeName));

#line default
#line hidden
            EndContext();
            BeginContext(1030, 32, true);
            WriteLiteral("</td>\n                    <td><a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 1062, "\"", 1101, 2);
            WriteAttributeValue("", 1069, "WorkType/Update/", 1069, 16, true);
#line 35 "/Users/fuhad/Desktop/projectAdminPanel/Views/WorkType/Index.cshtml"
WriteAttributeValue("", 1085, item.WorkTypeId, 1085, 16, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1102, 24, true);
            WriteLiteral(">Update</a> | <a href=\"\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 1126, "\"", 1162, 3);
            WriteAttributeValue("", 1136, "Delete(\'", 1136, 8, true);
#line 35 "/Users/fuhad/Desktop/projectAdminPanel/Views/WorkType/Index.cshtml"
WriteAttributeValue("", 1144, item.WorkTypeId, 1144, 16, false);

#line default
#line hidden
            WriteAttributeValue("", 1160, "\')", 1160, 2, true);
            EndWriteAttribute();
            BeginContext(1163, 43, true);
            WriteLiteral(">Delete</a></td>\n                    </tr>\n");
            EndContext();
#line 37 "/Users/fuhad/Desktop/projectAdminPanel/Views/WorkType/Index.cshtml"
                }

#line default
#line hidden
            BeginContext(1224, 197, true);
            WriteLiteral("            </table>\n        </div>\n        <!-- /.box-body -->\n        </div>\n        <!-- /.box -->\n    </div>\n    <!-- /.col -->\n    </div>\n    <!-- /.row -->\n</section>\n   \n<!-- DataTables -->\n");
            EndContext();
            BeginContext(1421, 85, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "6943f3f660ef052d4db2093f9f4c41b3ece684276865", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1506, 1, true);
            WriteLiteral("\n");
            EndContext();
            BeginContext(1507, 91, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "6943f3f660ef052d4db2093f9f4c41b3ece684278024", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1598, 323, true);
            WriteLiteral(@"

<!-- page script -->
<script>
    $(function () {
        $('#example1').DataTable();
    });
    function Delete(id){
        var txt;
        var r = confirm(""Are you sure you want to Delete?"");
        if (r == true) {
            
            $.ajax(
            {
                type: ""POST"",
                url: '");
            EndContext();
            BeginContext(1922, 32, false);
#line 66 "/Users/fuhad/Desktop/projectAdminPanel/Views/WorkType/Index.cshtml"
                 Write(Url.Action("Delete", "WorkType"));

#line default
#line hidden
            EndContext();
            BeginContext(1954, 553, true);
            WriteLiteral(@"',
                data: {
                    id: id
                },
                error: function (result) {
                    alert(""error"");
                },
                success: function (result) {
                    if (result == true) {
                        var baseUrl=""/WorkType"";
                        window.location.reload();
                    }
                    else {
                        alert(""There is a problem, Try Later!"");
                    }
                }
            });
        } 
    }
</script>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<projectAdminPanel.Models.WorkType>> Html { get; private set; }
    }
}
#pragma warning restore 1591
