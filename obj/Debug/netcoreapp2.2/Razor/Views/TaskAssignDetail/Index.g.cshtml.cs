#pragma checksum "/Users/fuhad/Desktop/projectAdminPanel/Views/TaskAssignDetail/Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8390a3776eb57a9d787becec5dbf8d8441509427"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_TaskAssignDetail_Index), @"mvc.1.0.view", @"/Views/TaskAssignDetail/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/TaskAssignDetail/Index.cshtml", typeof(AspNetCore.Views_TaskAssignDetail_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8390a3776eb57a9d787becec5dbf8d8441509427", @"/Views/TaskAssignDetail/Index.cshtml")]
    public class Views_TaskAssignDetail_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<projectAdminPanel.Models.TaskAssignDetail>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/bower_components/datatables.net/js/jquery.dataTables.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(62, 886, true);
            WriteLiteral(@"
<section class=""content-header"">
    <h1>
    Task Assign Detail
    </h1>
    <ol class=""breadcrumb"">
    <li><a href=""#""><i class=""fa fa-dashboard""></i> Home</a></li>
    <li class=""active"">Task Assign Detail</li>
    </ol>
</section>
<!-- Main content -->
<section class=""content"">
    <div class=""row"">
    <div class=""col-xs-12"">
        <div class=""box"">
        <div class=""box-header"">
            <h3 class=""box-title"">Manage Task Assign Detail</h3>
        </div>
        <!-- /.box-header -->
        <div class=""box-body"">
            <table id=""example1"" class=""table table-bordered table-striped"">
            <thead>
            <tr>
                <th>Id</th>
                <th>Starting</th>
                <th>Ending</th>
                <th>Task Description</th>
                <th>Task Completion</th>
            </tr>
            </thead>
            <tbody>
");
            EndContext();
#line 33 "/Users/fuhad/Desktop/projectAdminPanel/Views/TaskAssignDetail/Index.cshtml"
                 foreach (var item in Model)
                {

#line default
#line hidden
            BeginContext(1011, 49, true);
            WriteLiteral("                    <tr>\n                    <td>");
            EndContext();
            BeginContext(1061, 53, false);
#line 36 "/Users/fuhad/Desktop/projectAdminPanel/Views/TaskAssignDetail/Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TaskAssignDetailId));

#line default
#line hidden
            EndContext();
            BeginContext(1114, 30, true);
            WriteLiteral("</td>\n                    <td>");
            EndContext();
            BeginContext(1145, 43, false);
#line 37 "/Users/fuhad/Desktop/projectAdminPanel/Views/TaskAssignDetail/Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Starting));

#line default
#line hidden
            EndContext();
            BeginContext(1188, 30, true);
            WriteLiteral("</td>\n                    <td>");
            EndContext();
            BeginContext(1219, 41, false);
#line 38 "/Users/fuhad/Desktop/projectAdminPanel/Views/TaskAssignDetail/Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Ending));

#line default
#line hidden
            EndContext();
            BeginContext(1260, 30, true);
            WriteLiteral("</td>\n                    <td>");
            EndContext();
            BeginContext(1291, 50, false);
#line 39 "/Users/fuhad/Desktop/projectAdminPanel/Views/TaskAssignDetail/Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TaskDescription));

#line default
#line hidden
            EndContext();
            BeginContext(1341, 30, true);
            WriteLiteral("</td>\n                    <td>");
            EndContext();
            BeginContext(1372, 52, false);
#line 40 "/Users/fuhad/Desktop/projectAdminPanel/Views/TaskAssignDetail/Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.CompletionPercent));

#line default
#line hidden
            EndContext();
            BeginContext(1424, 32, true);
            WriteLiteral("</td>\n                    <td><a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 1456, "\"", 1511, 2);
            WriteAttributeValue("", 1463, "TaskAssignDetail/Update/", 1463, 24, true);
#line 41 "/Users/fuhad/Desktop/projectAdminPanel/Views/TaskAssignDetail/Index.cshtml"
WriteAttributeValue("", 1487, item.TaskAssignDetailId, 1487, 24, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1512, 24, true);
            WriteLiteral(">Update</a> | <a href=\"\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 1536, "\"", 1580, 3);
            WriteAttributeValue("", 1546, "Delete(\'", 1546, 8, true);
#line 41 "/Users/fuhad/Desktop/projectAdminPanel/Views/TaskAssignDetail/Index.cshtml"
WriteAttributeValue("", 1554, item.TaskAssignDetailId, 1554, 24, false);

#line default
#line hidden
            WriteAttributeValue("", 1578, "\')", 1578, 2, true);
            EndWriteAttribute();
            BeginContext(1581, 43, true);
            WriteLiteral(">Delete</a></td>\n                    </tr>\n");
            EndContext();
#line 43 "/Users/fuhad/Desktop/projectAdminPanel/Views/TaskAssignDetail/Index.cshtml"
                }

#line default
#line hidden
            BeginContext(1642, 197, true);
            WriteLiteral("            </table>\n        </div>\n        <!-- /.box-body -->\n        </div>\n        <!-- /.box -->\n    </div>\n    <!-- /.col -->\n    </div>\n    <!-- /.row -->\n</section>\n   \n<!-- DataTables -->\n");
            EndContext();
            BeginContext(1839, 85, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "8390a3776eb57a9d787becec5dbf8d84415094278318", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1924, 1, true);
            WriteLiteral("\n");
            EndContext();
            BeginContext(1925, 91, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "8390a3776eb57a9d787becec5dbf8d84415094279477", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2016, 323, true);
            WriteLiteral(@"

<!-- page script -->
<script>
    $(function () {
        $('#example1').DataTable();
    });
    function Delete(id){
        var txt;
        var r = confirm(""Are you sure you want to Delete?"");
        if (r == true) {
            
            $.ajax(
            {
                type: ""POST"",
                url: '");
            EndContext();
            BeginContext(2340, 40, false);
#line 72 "/Users/fuhad/Desktop/projectAdminPanel/Views/TaskAssignDetail/Index.cshtml"
                 Write(Url.Action("Delete", "TaskAssignDetail"));

#line default
#line hidden
            EndContext();
            BeginContext(2380, 561, true);
            WriteLiteral(@"',
                data: {
                    id: id
                },
                error: function (result) {
                    alert(""error"");
                },
                success: function (result) {
                    if (result == true) {
                        var baseUrl=""/TaskAssignDetail"";
                        window.location.reload();
                    }
                    else {
                        alert(""There is a problem, Try Later!"");
                    }
                }
            });
        } 
    }
</script>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<projectAdminPanel.Models.TaskAssignDetail>> Html { get; private set; }
    }
}
#pragma warning restore 1591
