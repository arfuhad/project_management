#pragma checksum "/Users/fuhad/Desktop/projectAdminPanel/Views/Shared/_MainMenu.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "dc4d026a03aba33ee4144a53b2fa584d4cba22be"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__MainMenu), @"mvc.1.0.view", @"/Views/Shared/_MainMenu.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/_MainMenu.cshtml", typeof(AspNetCore.Views_Shared__MainMenu))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"dc4d026a03aba33ee4144a53b2fa584d4cba22be", @"/Views/Shared/_MainMenu.cshtml")]
    public class Views_Shared__MainMenu : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 6401, true);
            WriteLiteral(@"<ul class=""sidebar-menu"" data-widget=""tree"">
<li class=""header"">HEADER</li>
<!-- Optionally, you can add icons to the links -->
<li class=""active""><a href=""/""><i class=""fa fa-dashboard""></i> <span>Home</span></a></li>
<li class=""treeview"">
    <a href=""#""><i class=""fa fa-university""></i> <span>Organization</span>
    <span class=""pull-right-container"">
        <i class=""fa fa-angle-left pull-right""></i>
        </span>
    </a>
    <ul class=""treeview-menu"">
        <li class=""treeview"">
            <a href=""#""><i class=""fa fa-university""></i> <span>Organization Type</span>
            <span class=""pull-right-container"">
                <i class=""fa fa-angle-left pull-right""></i>
                </span>
            </a>
            <ul class=""treeview-menu"">
                <li><a href=""/OrganizationType/Create"">Create Organization Type</a></li>
                <li><a href=""/OrganizationType"">All Organization Type</a></li>
            </ul>
        </li>
        <li><a href=""/Organization/Create"">Create Organ");
            WriteLiteral(@"ization</a></li>
        <li><a href=""/Organization"">All Organization</a></li>
    </ul>
</li>
<li class=""treeview"">
    <a href=""#""><i class=""fa fa-users""></i> <span>Resource</span>
    <span class=""pull-right-container"">
        <i class=""fa fa-angle-left pull-right""></i>
        </span>
    </a>
    <ul class=""treeview-menu"">
        <li class=""treeview"">
            <a href=""#""><i class=""fa fa-users""></i> <span>Resource Type</span>
            <span class=""pull-right-container"">
                <i class=""fa fa-angle-left pull-right""></i>
                </span>
            </a>
            <ul class=""treeview-menu"">
                <li><a href=""/ResourceType/Create"">Create Resource Type</a></li>
                <li><a href=""/ResourceType"">All Resource Type</a></li>
            </ul>
        </li>
        <li class=""treeview"">
            <a href=""#""><i class=""fa fa-users""></i> <span>Resource Priority</span>
            <span class=""pull-right-container"">
                <i class=""fa fa-angle-left pull-rig");
            WriteLiteral(@"ht""></i>
                </span>
            </a>
            <ul class=""treeview-menu"">
                <li><a href=""/ResourcePriority/Create"">Create Resource Priority</a></li>
                <li><a href=""/ResourcePriority"">All Resource Priority</a></li>
            </ul>
        </li>
        <li><a href=""/Resource/Create"">Create Resource</a></li>
        <li><a href=""/Resource"">All Resource</a></li>
    </ul>
</li>
<li class=""treeview"">
    <a href=""#""><i class=""fa fa-area-chart""></i> <span>Project</span>
    <span class=""pull-right-container"">
        <i class=""fa fa-angle-left pull-right""></i>
        </span>
    </a>
    <ul class=""treeview-menu"">
        <li class=""treeview"">
            <a href=""#""><i class=""fa fa-area-chart""></i> <span>Project Type</span>
            <span class=""pull-right-container"">
                <i class=""fa fa-angle-left pull-right""></i>
                </span>
            </a>
            <ul class=""treeview-menu"">
                <li><a href=""/ProjectType/Create"">Create Pro");
            WriteLiteral(@"ject Type</a></li>
                <li><a href=""/ProjectType"">All Project Type</a></li>
            </ul>
        </li>
        <li><a href=""/Project/Create"">Create Project</a></li>
        <li><a href=""/Project"">All Project</a></li>
    </ul>
</li>
<li class=""treeview"">
    <a href=""#""><i class=""fa fa-wrench""></i> <span>Work</span>
    <span class=""pull-right-container"">
        <i class=""fa fa-angle-left pull-right""></i>
        </span>
    </a>
    <ul class=""treeview-menu"">
        <li class=""treeview"">
            <a href=""#""><i class=""fa fa-wrench""></i> <span>Work Type</span>
            <span class=""pull-right-container"">
                <i class=""fa fa-angle-left pull-right""></i>
                </span>
            </a>
            <ul class=""treeview-menu"">
                <li><a href=""/WorkType/Create"">Create Work Type</a></li>
                <li><a href=""/WorkType"">All Work Type</a></li>
            </ul>
        </li>
        <li class=""treeview"">
            <a href=""#""><i class=""fa fa-wrench""><");
            WriteLiteral(@"/i> <span>Work Assign</span>
            <span class=""pull-right-container"">
                <i class=""fa fa-angle-left pull-right""></i>
                </span>
            </a>
            <ul class=""treeview-menu"">
                <li><a href=""/WorkAssign/Create"">Create Work Assign</a></li>
                <li><a href=""/WorkAssign"">All Work Assign</a></li>
            </ul>
        </li>
        <li><a href=""/Work/Create"">Create Work</a></li>
        <li><a href=""/Work"">All Work</a></li>
    </ul>
</li>
<li class=""treeview"">
    <a href=""#""><i class=""fa fa-tasks""></i> <span>Task</span>
    <span class=""pull-right-container"">
        <i class=""fa fa-angle-left pull-right""></i>
        </span>
    </a>
    <ul class=""treeview-menu"">
        <li class=""treeview"">
            <a href=""#""><i class=""fa fa-tasks""></i> <span>Task Type</span>
            <span class=""pull-right-container"">
                <i class=""fa fa-angle-left pull-right""></i>
                </span>
            </a>
            <ul class=""tree");
            WriteLiteral(@"view-menu"">
                <li><a href=""/TaskType/Create"">Create Task Type</a></li>
                <li><a href=""/TaskType"">All Task Type</a></li>
            </ul>
        </li>
        <li class=""treeview"">
            <a href=""#""><i class=""fa fa-tasks""></i> <span>Task Assign</span>
            <span class=""pull-right-container"">
                <i class=""fa fa-angle-left pull-right""></i>
                </span>
            </a>
            <ul class=""treeview-menu"">
                <li><a href=""/TaskAssign/Create"">Create Task Assign</a></li>
                <li><a href=""/TaskAssign"">All Task Assign</a></li>
            </ul>
        </li>
        <li class=""treeview"">
            <a href=""#""><i class=""fa fa-tasks""></i> <span>Task Assign Details</span>
            <span class=""pull-right-container"">
                <i class=""fa fa-angle-left pull-right""></i>
                </span>
            </a>
            <ul class=""treeview-menu"">
                <li><a href=""/TaskAssignDetail/Create"">Create Task Ass");
            WriteLiteral(@"ign Details</a></li>
                <li><a href=""/TaskAssignDetail"">All Task Assign Details</a></li>
            </ul>
        </li>
        <li><a href=""/Task/Create"">Create Task</a></li>
        <li><a href=""/Task"">All Task</a></li>
    </ul>
</li>
</ul>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
