using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using projectAdminPanel.Models;

namespace projectAdminPanel.Controllers
{
    public class OrganizationController : Controller
    {
        private readonly MyDbContext db;
        private readonly OrganizationViewModel viewModel;

        public OrganizationController(MyDbContext context)
        {
            db = context;
            viewModel = new OrganizationViewModel(db);
        }
        public ActionResult Index()
        {
            return View(db.Organization.ToList());
        }

        public ActionResult Create()
        {
            ViewBag.type = new SelectList(viewModel.OrganizationTypeList, "OrgTypeId", "OrgTypeName");
            return View();
        }

        [HttpPost]
        public ActionResult CreateOrganization(Organization organization)
        {
            if (organization.GetType() == typeof(Organization))
            {
                System.Console.WriteLine("this is Organization");
            }else
            {
                System.Console.WriteLine("this is not Organization" + organization.GetType());
            }
            System.Console.WriteLine(organization.ToString());
            db.Organization.Add(organization);
            db.SaveChanges();
            return RedirectToAction("Index", "Organization");
        }

        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                Organization organization = db.Organization.Where(s => s.OrgId == id).First();
                db.Organization.Remove(organization);
                db.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }

        public ActionResult Update(int id)
        {
            ViewBag.type = new SelectList(viewModel.OrganizationTypeList, "OrgTypeId", "OrgTypeName");
            return View(db.Organization.Where(s => s.OrgId == id).First());
        }

        [HttpPost]
        public ActionResult UpdateOrganization(Organization organization)
        {
            System.Console.WriteLine(organization.ToString());
            Organization d = db.Organization.Where(s => s.OrgId == organization.OrgId).First();
            d.OrgName = organization.OrgName;
            d.OrgTypeId = organization.OrgTypeId;
            db.SaveChanges();
            return RedirectToAction("Index", "Organization");
        }
    }
}