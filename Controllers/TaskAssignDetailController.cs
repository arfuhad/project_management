using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using projectAdminPanel.Models;

namespace projectAdminPanel.Controllers
{
    public class TaskAssignDetailController : Controller
    {
        private readonly MyDbContext db;
        private readonly TaskAssignDetailViewModel viewModel;

        public TaskAssignDetailController(MyDbContext context)
        {
            db = context;
            viewModel = new TaskAssignDetailViewModel(db);
        }
        public ActionResult Index()
        {
            return View(db.TaskAssignDetail.ToList());
        }

        public ActionResult Create()
        {
            var temp = viewModel.TaskAssignList;
            List<TempResourceMapModel> values = new List<TempResourceMapModel>();
            if(temp != null){
                System.Console.WriteLine(temp.ToString());
                foreach(var item in temp){
                    string x = "";
                    foreach(var item2 in viewModel.ResourcesList){
                        if(item.ResId == item2.ResId){
                            x =item2.ResName;
                        }
                    }
                    values.Add(new TempResourceMapModel(item.TaskAssignId,x));
                }
            }
            
            ViewBag.task = new SelectList(values, "Id", "Value");
            ViewBag.date = DateTime.Now;
            return View();
        }

        [HttpPost]
        public ActionResult CreateTaskAssignDetail(TaskAssignDetail taskAssignDetail)
        {
            if (taskAssignDetail.GetType() == typeof(TaskAssignDetail))
            {
                System.Console.WriteLine("this is Task Assign Detail");
            }
            else
            {
                System.Console.WriteLine("this is not Task Assign Detail" + taskAssignDetail.GetType());
            }
            System.Console.WriteLine(taskAssignDetail.ToString());
            db.TaskAssignDetail.Add(taskAssignDetail);
            db.SaveChanges();
            return RedirectToAction("Index", "TaskAssignDetail");
        }

        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                TaskAssignDetail taskAssignDetail = db.TaskAssignDetail.Where(s => s.TaskAssignDetailId == id).First();
                db.TaskAssignDetail.Remove(taskAssignDetail);
                db.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }

        public ActionResult Update(int id)
        {
            ViewBag.task = new SelectList(viewModel.TaskAssignList, "TaskAssignId", "Res.ResName");
            return View(db.TaskAssignDetail.Where(s => s.TaskAssignDetailId == id).First());
        }

        [HttpPost]
        public ActionResult UpdateTaskAssignDetail(TaskAssignDetail taskAssignDetail)
        {
            System.Console.WriteLine(taskAssignDetail.ToString());
            TaskAssignDetail d = db.TaskAssignDetail.Where(s => s.TaskAssignDetailId == taskAssignDetail.TaskAssignDetailId).First();
            d.Starting = taskAssignDetail.Starting;
            d.Ending = taskAssignDetail.Ending;
            d.TaskDescription = taskAssignDetail.TaskDescription;
            d.CompletionPercent = taskAssignDetail.CompletionPercent;
            d.TaskAssignId = taskAssignDetail.TaskAssignId;
            db.SaveChanges();
            return RedirectToAction("Index", "TaskAssignDetail");
        }
    }
}