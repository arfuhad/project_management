using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using projectAdminPanel.Models;

namespace AdminPanelTutorial
{
    public class HomeController : Controller
    {
        private readonly MyDbContext db ;

        public HomeController(MyDbContext context)
        {
            db = context;
        }
        public IActionResult Index()
        {
            DashboardViewModel dashboard = new DashboardViewModel();

            dashboard.Res_count = db.Resource.Count();
            dashboard.Org_count = db.Organization.Count();
            dashboard.Proj_count = db.Project.Count();
            dashboard.Task_count = db.Task.Count();
            dashboard.Work_count = db.Work.Count();

            return View(dashboard);
        }
    }
}