using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using projectAdminPanel.Models;

namespace projectAdminPanel.Controllers
{
    public class TaskController : Controller
    {
        private readonly MyDbContext db;
        private readonly TaskViewModel viewModel;

        public TaskController(MyDbContext context)
        {
            db = context;
            viewModel = new TaskViewModel(db);
        }
        public ActionResult Index()
        {
            return View(db.Task.ToList());
        }

        public ActionResult Create()
        {
            ViewBag.work = new SelectList(viewModel.WorkList, "WorkId", "WorkDescirption");
            ViewBag.type = new SelectList(viewModel.TaskTypeList, "TaskTypeId", "TaskTypeName");
            return View();
        }

        [HttpPost]
        public ActionResult CreateTask(Task task)
        {
            if (task.GetType() == typeof(Task))
            {
                System.Console.WriteLine("this is Task");
            }
            else
            {
                System.Console.WriteLine("this is not Task" + task.GetType());
            }
            System.Console.WriteLine(task.ToString());
            db.Task.Add(task);
            db.SaveChanges();
            return RedirectToAction("Index", "Task");
        }

        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                Task task = db.Task.Where(s => s.TaskId == id).First();
                db.Task.Remove(task);
                db.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }

        public ActionResult Update(int id)
        {
            ViewBag.work = new SelectList(viewModel.WorkList, "WorkId", "WorkDescirption");
            ViewBag.type = new SelectList(viewModel.TaskTypeList, "TaskTypeId", "TaskTypeName");
            return View(db.Task.Where(s => s.TaskId == id).First());
        }

        [HttpPost]
        public ActionResult UpdateTask(Task task)
        {
            System.Console.WriteLine(task.ToString());
            Task d = db.Task.Where(s => s.TaskId == task.TaskId).First();
            d.TaskDescirption = task.TaskDescirption;
            d.TaskDate = task.TaskDate;
            d.WorkId = task.WorkId;
            d.TaskTypeId = task.TaskTypeId;
            db.SaveChanges();
            return RedirectToAction("Index", "Task");
        }
    }
}