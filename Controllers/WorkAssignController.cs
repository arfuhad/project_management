using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using projectAdminPanel.Models;

namespace projectAdminPanel.Controllers
{
    public class WorkAssignController : Controller
    {
        private readonly MyDbContext db;
        private readonly WorkAssignViewModel viewModel;

        public WorkAssignController(MyDbContext context)
        {
            db = context;
            viewModel = new WorkAssignViewModel(db);
        }
        public ActionResult Index()
        {
            ViewBag.resType = viewModel.ResourceTypeList;
            return View(db.WorkAssign.ToList());
        }

        public ActionResult Create()
        {
            ViewBag.work = new SelectList(viewModel.WorkList, "WorkId", "WorkDescirption");
            ViewBag.res = new SelectList(viewModel.ResourceList, "ResId", "ResName");
            return View();
        }

        [HttpPost]
        public ActionResult CreateWorkAssign(WorkAssign workAssign)
        {
            if (workAssign.GetType() == typeof(WorkAssign))
            {
                System.Console.WriteLine("this is Work Assign");
            }
            else
            {
                System.Console.WriteLine("this is not Work Assign" + workAssign.GetType());
            }
            System.Console.WriteLine(workAssign.ToString());
            db.WorkAssign.Add(workAssign);
            db.SaveChanges();
            return RedirectToAction("Index", "WorkAssign");
        }

        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                WorkAssign workAssign = db.WorkAssign.Where(s => s.WorkAssignId == id).First();
                db.WorkAssign.Remove(workAssign);
                db.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }

        public ActionResult Update(int id)
        {
            ViewBag.work = new SelectList(viewModel.WorkList, "WorkId", "WorkDescirption");
            ViewBag.res = new SelectList(viewModel.ResourceList, "ResId", "ResName");
            return View(db.WorkAssign.Where(s => s.WorkAssignId == id).First());
        }

        [HttpPost]
        public ActionResult UpdateWorkAssign(WorkAssign workAssign)
        {
            System.Console.WriteLine(workAssign.ToString());
            WorkAssign d = db.WorkAssign.Where(s => s.WorkAssignId == workAssign.WorkAssignId).First();
            d.WorkAssignDate = workAssign.WorkAssignDate;
            d.WorkId = workAssign.WorkId;
            d.ResId = workAssign.ResId;
            db.SaveChanges();
            return RedirectToAction("Index", "WorkAssign");
        }
    }
}