using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using projectAdminPanel.Models;

namespace projectAdminPanel.Controllers
{
    public class WorkController : Controller
    {
        private readonly MyDbContext db;
        private readonly WorkViewModel viewModel;

        public WorkController(MyDbContext context)
        {
            db = context;
            viewModel = new WorkViewModel(db);
        }
        public ActionResult Index()
        {
            return View(db.Work.ToList());
        }

        public ActionResult Create()
        {
            ViewBag.proj = new SelectList(viewModel.ProjectList, "ProjId", "ProjName");
            ViewBag.type = new SelectList(viewModel.WorkTypeList, "WorkTypeId", "WorkTypeName");
            return View();
        }

        [HttpPost]
        public ActionResult CreateWork(Work work)
        {
            if (work.GetType() == typeof(Work))
            {
                System.Console.WriteLine("this is Work");
            }
            else
            {
                System.Console.WriteLine("this is not Work" + work.GetType());
            }
            System.Console.WriteLine(work.ToString());
            db.Work.Add(work);
            db.SaveChanges();
            return RedirectToAction("Index", "Work");
        }

        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                Work work = db.Work.Where(s => s.WorkId == id).First();
                db.Work.Remove(work);
                db.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }

        public ActionResult Update(int id)
        {
            ViewBag.proj = new SelectList(viewModel.ProjectList, "ProjId", "ProjName");
            ViewBag.type = new SelectList(viewModel.WorkTypeList, "WorkTypeId", "WorkTypeName");
            return View(db.Work.Where(s => s.WorkId == id).First());
        }

        [HttpPost]
        public ActionResult UpdateWork(Work work)
        {
            System.Console.WriteLine(work.ToString());
            Work d = db.Work.Where(s => s.WorkId == work.WorkId).First();
            d.WorkDescirption = work.WorkDescirption;
            d.WorkDate = work.WorkDate;
            d.ProjId = work.ProjId;
            d.WorkTypeId = work.WorkTypeId;
            db.SaveChanges();
            return RedirectToAction("Index", "Work");
        }
    }
}