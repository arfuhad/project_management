using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using projectAdminPanel.Models;

namespace projectAdminPanel.Controllers
{
    public class ProjectController : Controller
    {
        private readonly MyDbContext db;
        private readonly ProjectViewModel viewModel;

        public ProjectController(MyDbContext context)
        {
            db = context;
            viewModel = new ProjectViewModel(db);
        }
        public ActionResult Index()
        {
            return View(db.Project.ToList());
        }

        public ActionResult Create()
        {
            ViewBag.type = new SelectList(viewModel.ProjectTypeList, "ProjTypeId", "ProjTypeName");
            ViewBag.org = new SelectList(viewModel.OrganizationList, "OrgId", "OrgName");
            return View();
        }

        [HttpPost]
        public ActionResult CreateProject(Project project)
        {
            if (project.GetType() == typeof(Project))
            {
                System.Console.WriteLine("this is Project");
            }else
            {
                System.Console.WriteLine("this is not Project" + project.GetType());
            }
            System.Console.WriteLine(project.ToString());
            db.Project.Add(project);
            db.SaveChanges();
            return RedirectToAction("Index", "Project");
        }

        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                Project project = db.Project.Where(s => s.ProjId == id).First();
                db.Project.Remove(project);
                db.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }

        public ActionResult Update(int id)
        {
            ViewBag.type = new SelectList(viewModel.ProjectTypeList, "ProjTypeId", "ProjTypeName");
            ViewBag.org = new SelectList(viewModel.OrganizationList, "OrgId", "OrgName");
            return View(db.Project.Where(s => s.ProjId == id).First());
        }

        [HttpPost]
        public ActionResult UpdateProject(Project project)
        {
            System.Console.WriteLine(project.ToString());
            Project d = db.Project.Where(s => s.ProjId == project.ProjId).First();
            d.ProjName = project.ProjName;
            d.ProjDetails = project.ProjDetails;
            d.ProjTypeId = project.ProjTypeId;
            d.OrgId = project.OrgId;
            db.SaveChanges();
            return RedirectToAction("Index", "Project");
        }
    }
}