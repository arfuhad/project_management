using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using projectAdminPanel.Models;

namespace projectAdminPanel.Controllers
{
    public class ResourcePriorityController : Controller
    {
        private readonly MyDbContext db;

        public ResourcePriorityController(MyDbContext context)
        {
            db = context;
        }
        public ActionResult Index()
        {
            return View(db.ResourceHierarchy.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateResourcePriority(ResourceHierarchy resourceHierarchy)
        {
            if (resourceHierarchy.GetType() == typeof(ResourceHierarchy))
            {
                System.Console.WriteLine("this is Resourcetype");
            }
            else
            {
                System.Console.WriteLine("this is not Resourcetype" + resourceHierarchy.GetType());
            }
            System.Console.WriteLine(resourceHierarchy.ToString());
            db.ResourceHierarchy.Add(resourceHierarchy);
            db.SaveChanges();
            return RedirectToAction("Index", "ResourcePriority");
        }

        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                ResourceHierarchy resourceHierarchy = db.ResourceHierarchy.Where(s => s.ResHierId == id).First();
                db.ResourceHierarchy.Remove(resourceHierarchy);
                db.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }

        public ActionResult Update(int id)
        {
            return View(db.ResourceHierarchy.Where(s => s.ResHierId == id).First());
        }

        [HttpPost]
        public ActionResult UpdateResourcePriority(ResourceHierarchy resourceHierarchy)
        {
            System.Console.WriteLine(resourceHierarchy.ToString());
            ResourceHierarchy d = db.ResourceHierarchy.Where(s => s.ResHierId == resourceHierarchy.ResHierId).First();
            d.ResHierName = resourceHierarchy.ResHierName;
            db.SaveChanges();
            return RedirectToAction("Index", "ResourcePriority");
        }
    }
}