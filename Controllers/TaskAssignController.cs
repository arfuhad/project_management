using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using projectAdminPanel.Models;

namespace projectAdminPanel.Controllers
{
    public class TaskAssignController : Controller
    {
        private readonly MyDbContext db;
        private readonly TaskAssignViewModel viewModel;

        public TaskAssignController(MyDbContext context)
        {
            db = context;
            viewModel = new TaskAssignViewModel(db);
        }
        public ActionResult Index()
        {
            return View(db.TaskAssign.ToList());
        }

        public ActionResult Create()
        {
            ViewBag.task = new SelectList(viewModel.TaskList, "TaskId", "TaskDescirption");
            ViewBag.res = new SelectList(viewModel.ResourceList, "ResId", "ResName");
            return View();
        }

        [HttpPost]
        public ActionResult CreateTaskAssign(TaskAssign taskAssign)
        {
            if (taskAssign.GetType() == typeof(TaskAssign))
            {
                System.Console.WriteLine("this is Task Assign");
            }
            else
            {
                System.Console.WriteLine("this is not Task Assign" + taskAssign.GetType());
            }
            System.Console.WriteLine(taskAssign.ToString());
            db.TaskAssign.Add(taskAssign);
            db.SaveChanges();
            return RedirectToAction("Index", "TaskAssign");
        }

        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                TaskAssign taskAssign = db.TaskAssign.Where(s => s.TaskAssignId == id).First();
                db.TaskAssign.Remove(taskAssign);
                db.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }

        public ActionResult Update(int id)
        {
            ViewBag.task = new SelectList(viewModel.TaskList, "TaskId", "TaskDescirption");
            ViewBag.res = new SelectList(viewModel.ResourceList, "ResId", "ResName");
            return View(db.TaskAssign.Where(s => s.TaskAssignId == id).First());
        }

        [HttpPost]
        public ActionResult UpdateTaskAssign(TaskAssign taskAssign)
        {
            System.Console.WriteLine(taskAssign.ToString());
            TaskAssign d = db.TaskAssign.Where(s => s.TaskAssignId == taskAssign.TaskAssignId).First();
            d.TaskAssignDate = taskAssign.TaskAssignDate;
            d.TaskId = taskAssign.TaskId;
            d.ResId = taskAssign.ResId;
            db.SaveChanges();
            return RedirectToAction("Index", "TaskAssign");
        }
    }
}