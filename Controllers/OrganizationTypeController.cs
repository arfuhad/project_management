using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using projectAdminPanel.Models;

namespace projectAdminPanel.Controllers
{
    public class OrganizationTypeController : Controller
    {
        private readonly MyDbContext db;

        public OrganizationTypeController(MyDbContext context)
        {
            db = context;
        }
        public ActionResult Index()
        {
            return View(db.OrganizationType.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateOrganizationType(OrganizationType organizationType)
        {
            if (organizationType.GetType() == typeof(OrganizationType))
            {
                System.Console.WriteLine("this is Organization");
            }
            else
            {
                System.Console.WriteLine("this is not Organization" + organizationType.GetType());
            }
            System.Console.WriteLine(organizationType.ToString());
            db.OrganizationType.Add(organizationType);
            db.SaveChanges();
            return RedirectToAction("Index", "OrganizationType");
        }

        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                OrganizationType organizationType = db.OrganizationType.Where(s => s.OrgTypeId == id).First();
                db.OrganizationType.Remove(organizationType);
                db.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }

        public ActionResult Update(int id)
        {
            return View(db.OrganizationType.Where(s => s.OrgTypeId == id).First());
        }

        [HttpPost]
        public ActionResult UpdateOrganizationType(OrganizationType organizationType)
        {
            System.Console.WriteLine(organizationType.ToString());
            OrganizationType d = db.OrganizationType.Where(s => s.OrgTypeId == organizationType.OrgTypeId).First();
            d.OrgTypeName = organizationType.OrgTypeName;
            db.SaveChanges();
            return RedirectToAction("Index", "OrganizationType");
        }
    }
}