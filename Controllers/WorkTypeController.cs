using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using projectAdminPanel.Models;

namespace projectAdminPanel.Controllers
{
    public class WorkTypeController : Controller
    {
        private readonly MyDbContext db;

        public WorkTypeController(MyDbContext context)
        {
            db = context;
        }
        public ActionResult Index()
        {
            return View(db.WorkType.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateWorkType(WorkType workType)
        {
            if (workType.GetType() == typeof(WorkType))
            {
                System.Console.WriteLine("this is Project Type");
            }
            else
            {
                System.Console.WriteLine("this is not Project type" + workType.GetType());
            }
            System.Console.WriteLine(workType.ToString());
            db.WorkType.Add(workType);
            db.SaveChanges();
            return RedirectToAction("Index", "WorkType");
        }

        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                WorkType workType = db.WorkType.Where(s => s.WorkTypeId == id).First();
                db.WorkType.Remove(workType);
                db.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }

        public ActionResult Update(int id)
        {
            return View(db.WorkType.Where(s => s.WorkTypeId == id).First());
        }

        [HttpPost]
        public ActionResult UpdateWorkType(WorkType workType)
        {
            System.Console.WriteLine(workType.ToString());
            WorkType d = db.WorkType.Where(s => s.WorkTypeId == workType.WorkTypeId).First();
            d.WorkTypeName = workType.WorkTypeName;
            db.SaveChanges();
            return RedirectToAction("Index", "WorkType");
        }
    }
}