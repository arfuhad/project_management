using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using projectAdminPanel.Models;

namespace projectAdminPanel.Controllers
{
    public class TaskTypeController : Controller
    {
        private readonly MyDbContext db;

        public TaskTypeController(MyDbContext context)
        {
            db = context;
        }
        public ActionResult Index()
        {
            return View(db.TaskType.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateTaskType(TaskType taskType)
        {
            if (taskType.GetType() == typeof(TaskType))
            {
                System.Console.WriteLine("this is Project Type");
            }
            else
            {
                System.Console.WriteLine("this is not Project type" + taskType.GetType());
            }
            System.Console.WriteLine(taskType.ToString());
            db.TaskType.Add(taskType);
            db.SaveChanges();
            return RedirectToAction("Index", "TaskType");
        }

        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                TaskType taskType = db.TaskType.Where(s => s.TaskTypeId == id).First();
                db.TaskType.Remove(taskType);
                db.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }

        public ActionResult Update(int id)
        {
            return View(db.TaskType.Where(s => s.TaskTypeId == id).First());
        }

        [HttpPost]
        public ActionResult UpdateTaskType(TaskType taskType)
        {
            System.Console.WriteLine(taskType.ToString());
            TaskType d = db.TaskType.Where(s => s.TaskTypeId == taskType.TaskTypeId).First();
            d.TaskTypeName = taskType.TaskTypeName;
            db.SaveChanges();
            return RedirectToAction("Index", "TaskType");
        }
    }
}