using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using projectAdminPanel.Models;

namespace projectAdminPanel.Controllers
{
    public class ResourceTypeController : Controller
    {
        private readonly MyDbContext db;

        public ResourceTypeController(MyDbContext context)
        {
            db = context;
        }
        public ActionResult Index()
        {
            return View(db.ResourceType.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateResourceType(ResourceType resourceType)
        {
            if (resourceType.GetType() == typeof(ResourceType))
            {
                System.Console.WriteLine("this is Resourcetype");
            }
            else
            {
                System.Console.WriteLine("this is not Resourcetype" + resourceType.GetType());
            }
            System.Console.WriteLine(resourceType.ToString());
            db.ResourceType.Add(resourceType);
            db.SaveChanges();
            return RedirectToAction("Index", "ResourceType");
        }

        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                ResourceType resourceType = db.ResourceType.Where(s => s.ResTypeId == id).First();
                db.ResourceType.Remove(resourceType);
                db.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }

        public ActionResult Update(int id)
        {
            return View(db.ResourceType.Where(s => s.ResTypeId == id).First());
        }

        [HttpPost]
        public ActionResult UpdateResourceType(ResourceType resourceType)
        {
            System.Console.WriteLine(resourceType.ToString());
            ResourceType d = db.ResourceType.Where(s => s.ResTypeId == resourceType.ResTypeId).First();
            d.ResTypeName = resourceType.ResTypeName;
            db.SaveChanges();
            return RedirectToAction("Index", "ResourceType");
        }
    }
}