using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using projectAdminPanel.Models;

namespace projectAdminPanel.Controllers
{
    public class ResourceController : Controller
    {
        private readonly MyDbContext db;
        private readonly ResourceViewModel viewModel;

        public ResourceController(MyDbContext context)
        {
            db = context;
            viewModel = new ResourceViewModel(db);
        }
        public ActionResult Index()
        {
            return View(db.Resource.ToList());
        }

        public ActionResult Create()
        {
            //var types = new List<SelectList>(); 
            ViewBag.type = new SelectList(viewModel.ResourceTypeList,"ResTypeId","ResTypeName");
            ViewBag.hier = new SelectList(viewModel.ResourceHierarchyList, "ResHierId", "ResHierName");
            ViewBag.org = new SelectList(viewModel.OrganizationList, "OrgId", "OrgName");
            return View();
        }

        [HttpPost]
        public ActionResult CreateResource(Resource resource)
        {
            if(resource.GetType() ==  typeof(Resource)){
                System.Console.WriteLine("this is resource");
            }else if (resource.GetType() == typeof(ResourceViewModel))
            {
                System.Console.WriteLine("this is resource view");
            }else{
                System.Console.WriteLine("this is not resource" + resource.GetType());
            }
            System.Console.WriteLine(resource.ToString());
            db.Resource.Add(resource);
            db.SaveChanges();
            return RedirectToAction("Index", "Resource");
        }

        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                Resource resource = db.Resource.Where(s => s.ResId == id).First();
                db.Resource.Remove(resource);
                db.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }

        public ActionResult Update(int id)
        {
            ViewBag.type = new SelectList(viewModel.ResourceTypeList, "ResTypeId", "ResTypeName");
            ViewBag.hier = new SelectList(viewModel.ResourceHierarchyList, "ResHierId", "ResHierName");
            ViewBag.org = new SelectList(viewModel.OrganizationList, "OrgId", "OrgName");
            return View(db.Resource.Where(s => s.ResId == id).First());
        }

        [HttpPost]
        public ActionResult UpdateResource(Resource resource)
        {
            System.Console.WriteLine(resource.ToString());
            Resource d = db.Resource.Where(s => s.ResId == resource.ResId).First();
            d.ResName = resource.ResName;
            d.ResEmail = resource.ResEmail;
            d.ResMobile = resource.ResMobile;
            d.OrgId = resource.OrgId;
            d.ResHierId = resource.ResHierId;
            d.ResTypeId = resource.ResTypeId;
            db.SaveChanges();
            return RedirectToAction("Index", "Resource");
        }
    }
}