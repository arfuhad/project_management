using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using projectAdminPanel.Models;

namespace projectAdminPanel.Controllers
{
    public class ProjectTypeController : Controller
    {
        private readonly MyDbContext db;

        public ProjectTypeController(MyDbContext context)
        {
            db = context;
        }
        public ActionResult Index()
        {
            return View(db.ProjectType.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateProjectType(ProjectType projectType)
        {
            if (projectType.GetType() == typeof(ProjectType))
            {
                System.Console.WriteLine("this is Project Type");
            }
            else
            {
                System.Console.WriteLine("this is not Project type" + projectType.GetType());
            }
            System.Console.WriteLine(projectType.ToString());
            db.ProjectType.Add(projectType);
            db.SaveChanges();
            return RedirectToAction("Index", "ProjectType");
        }

        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                ProjectType projectType = db.ProjectType.Where(s => s.ProjTypeId == id).First();
                db.ProjectType.Remove(projectType);
                db.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }

        public ActionResult Update(int id)
        {
            return View(db.ProjectType.Where(s => s.ProjTypeId == id).First());
        }

        [HttpPost]
        public ActionResult UpdateProjectType(ProjectType projectType)
        {
            System.Console.WriteLine(projectType.ToString());
            ProjectType d = db.ProjectType.Where(s => s.ProjTypeId == projectType.ProjTypeId).First();
            d.ProjTypeName = projectType.ProjTypeName;
            db.SaveChanges();
            return RedirectToAction("Index", "ProjectType");
        }
    }
}