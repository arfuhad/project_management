using System.Collections.Generic;
using System.Linq;
using projectAdminPanel.Models;

namespace projectAdminPanel.Models
{
    public class WorkViewModel
    {
        private readonly MyDbContext dbContext;

        public List<Project> ProjectList { get; private set; }
        public List<WorkType> WorkTypeList { get; private set; }
        public WorkViewModel(MyDbContext context)
        {
            dbContext = context;
            ProjectList = dbContext.Project.ToList();
            WorkTypeList = dbContext.WorkType.ToList();
        }
    }
}