using System.Collections.Generic;
using System.Linq;
using projectAdminPanel.Models;

namespace projectAdminPanel.Models
{
    public class OrganizationViewModel
    {
        private readonly MyDbContext dbContext;

        public List<OrganizationType> OrganizationTypeList { get; private set; }
        public OrganizationViewModel(MyDbContext context)
        {
            dbContext = context;
            OrganizationTypeList = dbContext.OrganizationType.ToList();
        }
    }
}