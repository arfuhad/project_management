using System.Collections.Generic;
using System.Linq;
using projectAdminPanel.Models;

namespace projectAdminPanel.Models
{
    public class WorkAssignViewModel
    {
        private readonly MyDbContext dbContext;

        public List<Work> WorkList { get; private set; }
        public List<Resource> ResourceList { get; private set; }
        public List<ResourceType> ResourceTypeList { get; private set;}
        public WorkAssignViewModel(MyDbContext context)
        {
            dbContext = context;
            WorkList = dbContext.Work.ToList();
            ResourceList = dbContext.Resource.ToList();
            ResourceTypeList = dbContext.ResourceType.ToList();
        }
    }
}