using System.Collections.Generic;
using System.Linq;
using projectAdminPanel.Models;

namespace projectAdminPanel.Models
{
    public class TaskAssignViewModel
    {
        private readonly MyDbContext dbContext;

        public List<Task> TaskList { get; private set; }
        public List<Resource> ResourceList { get; private set; }
        public TaskAssignViewModel(MyDbContext context)
        {
            dbContext = context;
            TaskList = dbContext.Task.ToList();
            ResourceList = dbContext.Resource.ToList();
        }
    }
}