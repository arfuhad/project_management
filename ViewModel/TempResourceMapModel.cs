namespace projectAdminPanel.Models
{
    public class TempResourceMapModel
    {
        public int Id {get; private set;}
        public string Value {get; private set;}
        public TempResourceMapModel(int Id, string Value)
        {
            this.Id = Id;
            this.Value = Value;
        }
    }
}