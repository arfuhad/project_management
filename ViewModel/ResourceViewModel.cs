using System.Collections.Generic;
using System.Linq;
using projectAdminPanel.Models;

namespace projectAdminPanel.Models
{
    public class ResourceViewModel
    {
        private readonly MyDbContext dbContext;

        public List<ResourceType> ResourceTypeList { get; private set;}
        public List<ResourceHierarchy> ResourceHierarchyList { get; private set; }

        public List<Organization> OrganizationList { get; private set; }
        public ResourceViewModel(MyDbContext context)
        {
            dbContext = context;
            ResourceTypeList = dbContext.ResourceType.ToList();
            ResourceHierarchyList = dbContext.ResourceHierarchy.ToList();
            OrganizationList = dbContext.Organization.ToList();
        }


    }
}