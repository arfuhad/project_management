using System.Collections.Generic;
using System.Linq;
using projectAdminPanel.Models;

namespace projectAdminPanel.Models
{
    public class TaskAssignDetailViewModel
    {
        private readonly MyDbContext dbContext;

        public List<TaskAssign> TaskAssignList { get; private set; }

        public List<Resource> ResourcesList { get; private set; }
        public TaskAssignDetailViewModel(MyDbContext context)
        {
            dbContext = context;
            TaskAssignList = dbContext.TaskAssign.ToList();
            ResourcesList = dbContext.Resource.ToList();
        }
    }
}