using System.Collections.Generic;
using System.Linq;
using projectAdminPanel.Models;

namespace projectAdminPanel.Models
{
    public class WorkAssignDetailViewModel
    {
        private readonly MyDbContext dbContext;

        public List<TaskAssign> TaskAssignList { get; private set; }
        public WorkAssignDetailViewModel(MyDbContext context)
        {
            dbContext = context;
            TaskAssignList = dbContext.TaskAssign.ToList();
        }
    }
}