namespace projectAdminPanel.Models
{
    public class DashboardViewModel
    {
        public int Org_count { get; set; }
        public int Proj_count { get; set; }
        public int Res_count { get; set; }
        public int Work_count { get; set; }
        public int Task_count { get; set; }

    }
}