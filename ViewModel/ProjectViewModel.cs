using System.Collections.Generic;
using System.Linq;
using projectAdminPanel.Models;

namespace projectAdminPanel.Models
{
    public class ProjectViewModel
    {
        private readonly MyDbContext dbContext;

        public List<ProjectType> ProjectTypeList { get; private set; }
        public List<Organization> OrganizationList {get; private set;}
        public ProjectViewModel(MyDbContext context)
        {
            dbContext = context;
            ProjectTypeList = dbContext.ProjectType.ToList();
            OrganizationList = dbContext.Organization.ToList();
        }
    }
}