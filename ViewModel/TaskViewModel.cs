using System.Collections.Generic;
using System.Linq;
using projectAdminPanel.Models;

namespace projectAdminPanel.Models
{
    public class TaskViewModel
    {
        private readonly MyDbContext dbContext;

        public List<Work> WorkList { get; private set; }
        public List<TaskType> TaskTypeList { get; private set; }
        public TaskViewModel(MyDbContext context)
        {
            dbContext = context;
            WorkList = dbContext.Work.ToList();
            TaskTypeList = dbContext.TaskType.ToList();
        }
    }
}